package edu.westga.cs6311.kiosk.model;

import java.util.ArrayList;

/**
 * Final Exam: unit tests for program
 * @author 	CS6311
 * @author 	jim perry, jr.
 * @version	10.17.2015
 */
public class Customer {

	private String customerName;
	private Double budget;
	private ArrayList<Computer> shoppingCart;	
	
	
	/**
	 * initialize the instance variable(s)  
	 * @param name customer name who is shopping
	 * @param budget amount of money customer has to spend
	 * @precondition name == null
	 * @precondition budget == null
	 **/
	public Customer(String name, Double budget) {
		if (name == null) {
			this.customerName = "";
		}
		if (budget == null) {
			this.budget = 0.0;
		}
		this.shoppingCart = new ArrayList<Computer>();
		this.customerName = name;
		this.budget = budget;
	}
	
	/**
	 * something
	 * @return returns string 
	 **/
	public String getName() {
		return this.customerName;
	}
	
	/**
	 * something
	 * @return returns string 
	 **/
	public Double getBudget() {
		return this.budget;
	}	
	
	/**
	 * return a String listing the complete information about each computer 
	 * to be purchased on its own line. If cart is empty, then method
	 * will return an appropriate String describing the  situation.
	 * @return returns string 
	 **/
	public String getCart() {
		String result = "";
		
		if (this.shoppingCart.isEmpty()) {
			return "\t There are no items in shopping cart";
		}
		for (Computer currentItem : this.shoppingCart) {	
			result += "\t > " + currentItem.getSku() + currentItem.getPrice() + "\n";	
		}		
		return result;
	}
	
	/**
	 * This method will check to be sure the Customer has enough
	 * money left to purchase the given quantity of computers. 
	 * If they do, then it will return true, otherwise it will return false.
	 * @param computer computer item 
	 * @return returns true if customer has enough money  
	 **/
	public Boolean enoughMoneyToBuy(Computer computer) {
		float totalCart = 0;
		return (totalCart < this.budget);
	}
	
	/**
	 * If customer has enough money and there are enough in stock
	 * to be purchased, add the appropriate computer to the cart. 
	 * remove computers from the inventory, reduce customer hudget 
	 * @param computer computer item 
	 * @param units to be purchased
	 * @preconditon if enoughMoneyToBuy = true && getStock() > units
	 **/
	public void addToCart(Computer computer, int units) {
		if (this.enoughMoneyToBuy(computer) && computer.getStock() > units) {
			this.shoppingCart.add(computer);
			this.budget = this.budget - (computer.getPrice() * units); 
			computer.setStock(computer.getStock() - units);
		} else {
			System.out.println("\t   > Not able to add to cart. Budget or stock too low.");
		}
	}
	
	/**
	 * This method will simply clear the shopping cart
	 **/
	public void completePurchase() {
		this.shoppingCart = new ArrayList<Computer>();
		System.out.println("\t  Purchased complete, shopping cart cleared");		
	}
	
	
	
}
