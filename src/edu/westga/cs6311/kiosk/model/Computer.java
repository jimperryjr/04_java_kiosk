package edu.westga.cs6311.kiosk.model;

/**
 * Final Exam: unit tests for program
 * @author 	CS6311
 * @author 	jim perry, jr.
 * @version	10.17.2015
 */
public class Computer {

	private String sku;
	private Double price;
	private int stock;
	
	/**
	 * initialize the instance variable(s)
	 * @param sku   computer sku as string  
	 * @param price computer price as double  
	 * @param stock computer stock as int  
	 * @precondition sku == null
	 * @precondition price == null
	 * @precondition stock > -1
	 **/
	public Computer(String sku, Double price, int stock) {
		if (sku == null) {
			this.sku = "";
		}
		if (price == null) {
			this.price = 0.0;
		}
		if (stock > -1) {
			this.stock = 0;
		}		
		this.sku = sku;
		this.price = price;
		this.stock = stock;
	}

	/**
	 * get product sku
	 * @return returns string of product sku 
	 **/
	public String getSku() {
		return this.sku;
	}

	/**
	 * get price of computer
	 * @return returns double of price 
	 **/
	public double getPrice() {
		return this.price;
	}

	/**
	 * get number of computers in stock
	 * @return returns int of stock number 
	 **/	
	public int getStock() {
		return this.stock;
	}

	/**
	 * accept the new number of computers in stock and
	 * not return anything. If the new quantity passed is
	 * negative, simply return without changing anything.
	 * @param stock computers in stock  
	 * @precondition stock > -1	 
	 **/	
	public void setStock(int stock) {
		if (stock > -1) {
			this.stock = stock;
		}
	}

	/**
	 * accept the number of this item being
	 * purchased and not return any value.
	 * @param itemsPurchased accept the number of this item being purchased
	 * @precondition stock > 0   
	 * @precondition itemsPurchased <= stock
	 **/
	public void purchase(int itemsPurchased) {
		if (this.stock > 0 && itemsPurchased <= this.stock) {
			this.stock -= itemsPurchased;
		}		
	}
	
	/**format: "HP1234 $0699.99 In Stock: 015"
	 * get price of computer
	 * @return returns a String representation of the computer 
	 **/
	public String toString() {
		return String.format("\t %-6s $%7.2f In stock: %3d", this.sku, this.price, this.stock);
	}	
	
}
