package edu.westga.cs6311.kiosk.model;

import java.util.ArrayList;

/**
 * Final Exam: unit tests for program
 * @author 	CS6311
 * @author 	jim perry, jr.
 * @version	10.17.2015
 */
public class InventoryManager {
	
	private ArrayList<Computer> computers;
	
	/**
	 * initialize the instance variable(s) 
	 **/
	public InventoryManager() {
		this.computers = new ArrayList<Computer>();
	}
	
	/**
	 * Adds the specified computer to the inventory
	 * @param			newComputer	the computer to be added
	 * @precondition	newComputer != null
	 */
	public void startStore(Computer newComputer) {
		if (newComputer != null) {
			this.computers.add(newComputer);
		}	
	}	

	/**
	 * Finds and returns the first student that matches the specified last name.
	 * @param 			sku	sku to search for
	 * @precondition	sku != null
	 * @return 			find sku or null if not found.
	 */	
	public Computer findComputer(String sku) {	
		if (sku == null) {
			return null;
		}

		for (Computer currentSku : this.computers) {
			if (currentSku.getSku().equalsIgnoreCase(sku)) {
				return currentSku;
			}
		}
		return null;		
	}
	
	/**
	 * create computer and add it to the inventory
	 * @param sku   computer sku as string  
	 * @param price computer price as double  
	 * @param stock computer stock as int  
	 **/
	public void addComputer(String sku, Double price, int stock) {	
		Computer newComputer = new Computer(sku, price, stock);	
		this.computers.add(newComputer);		
	}		
	
	
	/**
	 * This method will search the inventory and return
	 * the least expensive computer. If there are no 
	 * computers in stock, this will return null.
	 * @return returns least expensive computer.
	 **/
	public Double getLeastExpensive() {
		double smallest = this.getMostExpensive();
		for (int count = 0; count < this.computers.size(); count++) {
			if (this.computers.get(count).getPrice() < smallest) {
				smallest = this.computers.get(count).getPrice();
			}
		}
		return smallest;
	}		
	
	
	/**
	 * search the inventory and return the most expensive computer.
	 *  If there are no computers in stock, this will return null.
	 * @return returns the most expensive computer
	 **/
	public Double getMostExpensive() {
		double largest = 0;
		for (int count = 0; count < this.computers.size(); count++) {
			if (this.computers.get(count).getPrice() > largest) {
				largest = this.computers.get(count).getPrice();
			}
		}
		return largest;
	}	
	
	
	/**
	 * calculate and return the total number of computers in stock
	 * @return returns int of total number of computers in stock
	 **/
	public int getTotalInStock() {
		int totalInStock = 0;
		for (Computer currentItem : this.computers) {	
			totalInStock += currentItem.getStock();			
		}		
		return totalInStock;
	}	
	
	/**
	 * calculate and return the average computer cost. 
	 * If there are no computers in stock, then this will return 0.
	 * @return returns average computer cost 
	 **/
	public Float getAveragePrice() {
		float totalPrice = 0;
					
		for (Computer currentItem : this.computers) {	
			totalPrice += currentItem.getPrice();	
		}		
		return totalPrice / this.computers.size();				
	}
	
	/**
	 * generated entire inventory (format:  "HP1234 $0699.99 In Stock: 015")
	 * @return returns String of complete description of the inventory 
	 **/
	public String toString() {
		String result = "";
		
		if (this.computers.isEmpty()) {
			return "\t There are no computers in inventory";
		}
		for (Computer currentItem : this.computers) {	
			result += currentItem.toString() + "\n";	
		}
		result += "\n";
		result += String.format("\t Number of computer models:     %3d \n", this.computers.size());		
		result += String.format("\t quantity of all computers:     %3d \n", this.getTotalInStock());
		result += String.format("\t Most expensive computer:  $%7.2f \n", this.getMostExpensive());
		result += String.format("\t Least expensive computer: $%7.2f \n", this.getLeastExpensive());
		result += String.format("\t Average computer cost:    $%7.2f", this.getAveragePrice());
		
		return result;
	}	

}
