package edu.westga.cs6311.kiosk.view;

import java.util.Scanner;

import edu.westga.cs6311.kiosk.model.Computer;
import edu.westga.cs6311.kiosk.model.InventoryManager;

/**
 * Final Exam: unit tests for program
 * @author 	CS6311
 * @author 	jim perry, jr.
 * @version	10.17.2015
 */
public class ManagerTUI {
	
	private static final int MIN_MENU = 1;
	private static final int MAX_MENU = 4;
	
	private static final String WELCOME = "Welcome to The Great Purchase Kiosk Application!\n"
										+ "Please select a menu option by choosing a \n"
										+ "corresponding number between " + MIN_MENU + " and " + MAX_MENU + ".\n";
	
	private static final String MENU = "1 - Open new store \t 2 - Add new computer \n"
									 + "3 - View inventory \t 4 - Quit manager application \n";
	
	private static final String ENTER_SELECTION = "Enter selection: ";			 		
		
	private static final String SUCCESS_ENTRY = " successfully added";
	
	private static final String ERROR_NOT_INT = "Please enter a valid menu item between " + MIN_MENU + " and " + MAX_MENU + ". \n";
	private static final String ERROR_SKU =  "   > requires a SKU with max length of 6 \n\t SKU: ";
	private static final String ERROR_PRICE =  " ] is not valid, enter a price from 1.00 - 9999.99.";
	private static final String ERROR_STOCK =  " ] is not valid, enter a whole number between 1 to 999.";

	private static final String MENU_1 = "\n    [1 Open new store ]\n";
	private static final String MENU_2 = "\n    [2 Add new computer ]\n";
	private static final String MENU_3 = "\n    [3 View inventory ]\n";
	private static final String MENU_4 = "\n    [4 Returning to main kiosk ]\n";
	
	private static final String SPACE = "\n";


	
	
	
	private Scanner userKeyboard;
	private InventoryManager inventory;
	private int userchoice;
	
	/**
	 * initialize the instance variables
	 * @param 		 model accepts model of InventoryManager
	 * @precondition model != null
	 */
	public ManagerTUI(InventoryManager model) {
		this.userKeyboard = new Scanner(System.in);	
		
		if (model != null) {
			this.inventory = new InventoryManager();
		} 		
	}
	
	/**
	 * manages user interface by calling on 
	 * private helper methods as appropriate
	 */
	public void runManager() {		
		this.displayInstructions();	
		this.displayMenuCaptureInput();
	}	
	
	/**
	 * displays instructions 
	 */
	private void displayInstructions() {
		System.out.print(WELCOME + "\n");
	};
	
	/**
	 * prompt for user menu selection
	 */	
	private void displayMenuCaptureInput() {
		this.showMenu();
		this.askUserForMeuSelection();
		this.captureMenuSelection();
		this.matchSelectionToAppFeaturs();	
	}
	
	/**
	 * displays main menu 
	 */	
	private void showMenu() {
		System.out.print(MENU + "\n");		
	}
	
	/**
	 * prompt for user menu selection
	 */	
	private void askUserForMeuSelection() {
		System.out.print(ENTER_SELECTION);		
	}	
			
	/**
	 * prompt users and capture input 
	 */
	private void captureMenuSelection() {
		try {
			this.userchoice = Integer.parseInt(this.userKeyboard.nextLine());
		} catch (NumberFormatException error) {
			System.out.println("\nOur appoligies. [ " + this.getErrorString(error) + " ] is not a valid selection.");
		}
	}

	/**
	 * prompt users and capture input 
	 */
	private void matchSelectionToAppFeaturs() {
		switch (this.userchoice) {
			case 1:	
	        	System.out.println(MENU_1);
	        	this.startStore();
	        	this.displayMenuCaptureInput();
				break;
	        case 2: 
	        	System.out.println(MENU_2);
	        	this.addComputer();
	        	this.displayMenuCaptureInput();
	            break;
	        case 3: 
	        	System.out.println(MENU_3);	
	        	this.viewCurrentInventory();
	        	this.displayMenuCaptureInput();
	            break;
	        case 4: 	    		
	        	System.out.println(MENU_4);
	        	this.returnToMainKiosk();	        	
	            break;
	        default: 
	        	System.out.print(ERROR_NOT_INT);	        	
	        	System.out.println("");
	        	this.displayMenuCaptureInput();
	        	break;		
		}	

	}	

	/**
	 * starts a new store with standard items:
	 *  15 HP1234's that cost $699.99 each
		120 DL333's that cost $1,349.28 each
	 **/	
	private void startStore() {
		Computer computerInventory1 = new Computer("HP1234", 699.99, 15);		
		Computer computerInventory2 = new Computer("DL333", 1349.28, 120);	
		
		this.inventory.startStore(computerInventory1);
		this.inventory.startStore(computerInventory2);
	}
	
	/**
	 * adds a computer to inventory
	 * 
	 **/	
	private void addComputer() {
		String sku = "";
		Double price = 0.0;
		int stock = -1;				

		System.out.print("\t SKU: ");
		sku = this.userKeyboard.nextLine();
		
		while (sku.length() > 6) {
			System.out.print("\t" + ERROR_SKU);
			sku = this.userKeyboard.nextLine();			
		}

		while (!(price > 0 && price < 10000)) {
			System.out.print("\t Price: ");
			try {
				price = Double.parseDouble(this.userKeyboard.nextLine());
			} catch (NumberFormatException error) {
				System.out.println("\t   > Sorry: [ " + this.getErrorString(error) + ERROR_PRICE);
			}
        }				
		while (!(stock > 0 && stock < 1000)) {
			System.out.print("\t Stock: ");
			try {
				stock = Integer.parseInt(this.userKeyboard.nextLine());
			} catch (NumberFormatException error) {
				System.out.println("\t   > Sorry: [ " + this.getErrorString(error) + ERROR_STOCK);
			}	
        }		
		this.inventory.addComputer(sku, price, stock);		
		System.out.println("\t   >" + this.inventory.findComputer(sku) + SUCCESS_ENTRY);		
		System.out.println(SPACE);		
	}	
	
	/**
	 * displays inventory of computers
	 **/	
	private void viewCurrentInventory() {
		System.out.println(this.inventory.toString() + "\n");
	}

	/**
	 * return to main menu
	 **/	
	private void returnToMainKiosk() {
		KioskTUI viewKiosk = new KioskTUI(this.inventory);		
		viewKiosk.runKiosk();		
	}	
	
	/**
	 * provides specific text that causes input errors 
	 * @param error accepts ERROR from try catch 
	 * @return returns the value\input responsible for error
	 */
	private String getErrorString(NumberFormatException error) {
		int first = error.getMessage().indexOf("\"", 0) + 1; 
		int second = error.getMessage().lastIndexOf("\"");

		return error.getMessage().substring(first, second);		
	}

}
