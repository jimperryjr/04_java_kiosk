package edu.westga.cs6311.kiosk.view;

import java.util.Scanner;

import edu.westga.cs6311.kiosk.model.InventoryManager;

/**
 * Final Exam: unit tests for program
 * @author 	CS6311
 * @author 	jim perry, jr.
 * @version	10.17.2015
 */
public class KioskTUI {
	private static final String WELCOME = "Welcome to The Great Purchase Kiosk Application!\n"
										+ "Select a menu option between 1 and 3.\n";
	
	private static final String MENU = "1 - Start manager application  \t 3 - Quit \n"
									 + "2 - Start customer application \t \n";
	
	private static final String ENTER_SELECTION = "Enter selection: ";
	
	private static final String THANK_YOU = "Thank you for using the Kiosk application.\nHave a nice day.\n\n";
	
	private static final String ERROR_NOT_INT = "Please enter a valid menu item between 1 and 3. \n";

	private static final String MENU_1 = "\n    [1 manager application ]\n";
	private static final String MENU_2 = "\n    [2 customer application ]\n";
	private static final String MENU_3 = "\n    [3 Quit ]\n\n\n";


	
	private InventoryManager inventory;
	private Scanner userKeyboard;
	private int userchoice;
		
	/**
	 * accepts an InventoryManager object and initializes
     * the InventoryManager and Scanner instance variables
     * @param inventory 
	 **/
	public KioskTUI(InventoryManager inventory) {
		this.inventory = inventory;
		this.userKeyboard = new Scanner(System.in);			
	}	
	/**
	 * serves as the 'director' of the main user.
	 * allows user to choose to enter the manager's
	 * application or the shopper's application
	 **/
	public void runKiosk() {
		System.out.print(WELCOME + "\n");	
		this.displayMenuCaptureInput();		
	}
	
	/**
	 * prompt for user menu selection
	 */	
	private void displayMenuCaptureInput() {
		System.out.print(MENU + "\n");
		System.out.print(ENTER_SELECTION);
		this.captureMenuSelection();
		this.matchSelectionToAppFeaturs();	
	}
		
	/**
	 * prompt users and capture input 
	 */
	private void captureMenuSelection() {
		try {
			this.userchoice = Integer.parseInt(this.userKeyboard.nextLine());
		} catch (NumberFormatException error) {
			System.out.println("\nOur appoligies. [ " + this.getErrorString(error) + " ] is not a valid selection.");
		}
	}

	/**
	 * prompt users and capture input 
	 */
	private void matchSelectionToAppFeaturs() {
		switch (this.userchoice) {
			case 1:	
	        	System.out.println(MENU_1);
	        	this.startManagerApp();	        		  
				break;
	        case 2: 
	        	System.out.println(MENU_2);
	        	this.startCustomerApp();
	            break;
	        case 3: 	    		
	        	System.out.println(MENU_3);
	        	System.out.println(THANK_YOU);	        	
	            break;
	        default: 
	        	System.out.print(ERROR_NOT_INT);	        	
	        	System.out.println("");
	        	this.displayMenuCaptureInput();
	        	break;		
		}	
	}
	
	
	/**
	 * starts manager program
	 **/		
	public void startManagerApp() {
		ManagerTUI viewManager = new ManagerTUI(this.inventory);
		viewManager.runManager();		
	}
	
	/**
	 * starts customer program
	 **/	
	public void startCustomerApp() {
		CustomerTUI viewManager = new CustomerTUI(this.inventory);
		viewManager.runCustomer();	
	}		

	/**
	 * provides specific text that causes input errors 
	 * @param error accepts ERROR from try catch 
	 * @return returns the value\input responsible for error
	 */
	private String getErrorString(NumberFormatException error) {
		int first = error.getMessage().indexOf("\"", 0) + 1; 
		int second = error.getMessage().lastIndexOf("\"");

		return error.getMessage().substring(first, second);		
	}
	
	
	
}
