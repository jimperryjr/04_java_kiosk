package edu.westga.cs6311.kiosk.view;

import java.util.Scanner;
import edu.westga.cs6311.kiosk.model.Customer;
import edu.westga.cs6311.kiosk.model.InventoryManager;


/**
 * Final Exam: unit tests for program
 * @author 	CS6311
 * @author 	jim perry, jr.
 * @version	10.17.2015
 */
public class CustomerTUI {
	
	private static final int MIN_MENU = 1;
	private static final int MAX_MENU = 4;
	
	private static final double BUDGET_CUTOFF = 999999.99;
	
	private static final String WELCOME = "\t Hello valued customer!";
		
	private static final String MENU = "1 - View inventory \t\t 4 - View money remaining \n"
									 + "2 - Add computer to the cart  \t 5 - Checkout \n"
									 + "3 - View cart \t\t\t 6 - Quit shopper application \n";
	
	private static final String ENTER_SELECTION = "Enter selection: ";
	
	private static final String THANK_YOU = "\t > Thank you. Returning to main menu.\n";
		
	private static final String ENTER_NAME   = "\t Name:   ";			 		
	private static final String ENTER_BUDGET = "\t Budget: ";	
	
	private static final String ERROR_NOT_INT = "Please enter a valid menu item between " + MIN_MENU + " and " + MAX_MENU + ". \n";
	private static final String ERROR_BUDGET = "is not valid. \n\t   Enter a $ amount between 1 and " + BUDGET_CUTOFF + "\n";
	private static final String ERROR_SKU =  "   > requires a SKU with max length of 6 \n\t SKU: ";
	private static final String ERROR_PRICE =  " ] is not valid, enter a price from 1.00 - 9999.99.";
	
	private static final String SUCCESS_ENTRY = " your profile has been successfully created \n";	
	
	private static final String MENU_1 = "\n    [1 View inventory ]\n";
	private static final String MENU_2 = "\n    [2 Add computer to the cart ]\n";
	private static final String MENU_3 = "\n    [3 View cart ]\n";
	private static final String MENU_4 = "\n    [4 View money remaining ]\n";
	private static final String MENU_5 = "\n    [5 Checkout ]\n";
	private static final String MENU_6 = "\n    [6 Quit shopper application ]";
	
	private static final String SPACE = "\n";



	
	private InventoryManager inventory;
	private Customer customer;
	private Scanner userKeyboard;
	private int userchoice;	

	
	/**
	 * accepts an InventoryManager object and initializes
     * the InventoryManager and Scanner instance variables
     * @param inventory 
	 **/
	public CustomerTUI(InventoryManager inventory) {
		this.userKeyboard = new Scanner(System.in);

		if (inventory != null) {
			this.inventory = inventory;
		}
	}	
	
	/**
	 * serve as the 'director' of the user interface by calling
	 * on private helper methods to do their work, as appropriate.
	 **/
	public void runCustomer() {
		this.displayInstructions();	
		this.promptCaptureNameBudget();
		this.displayMenuCaptureInput();	
	}

	/**
	 * prompt the user for a name and an amount to spend. 
	 * initialize the Customer instance variable.
	 **/
	private void displayInstructions() {
		System.out.print(WELCOME + "\n");	
	}
		
	/**
	 * adds a computer to inventory
	 * 
	 **/	
	private void promptCaptureNameBudget() {
		String name = "";
		Double budget = 0.0;				

		while (name.equals("")) {
			System.out.print(ENTER_NAME);
			try {
				name = this.userKeyboard.nextLine();
			} catch (NumberFormatException error) {
				System.out.println("\t   > Sorry: [ " + this.getErrorString(error) + " ] " + ERROR_BUDGET);
			}
        }		

		while (!(budget > 0 && budget < BUDGET_CUTOFF)) {
			System.out.print(ENTER_BUDGET);
			try {
				budget = Double.parseDouble(this.userKeyboard.nextLine());
			} catch (NumberFormatException error) {
				System.out.println("\t   > Sorry: [ " + this.getErrorString(error) + " ] " + ERROR_BUDGET);
			}
        }			
		this.customer = new Customer(name, budget);
		
		System.out.println("\t   >" + SUCCESS_ENTRY);		
		//System.out.println(SPACE);		
	}

	/**
	 * prompt for user menu selection
	 */	
	private void displayMenuCaptureInput() {
		this.showMenu();
		this.askUserForMeuSelection();
		this.captureMenuSelection();
		this.matchSelectionToAppFeaturs();	
	}
		
	/**
	 * displays main menu 
	 */	
	private void showMenu() {
		System.out.print(MENU + "\n");		
	}
	
	/**
	 * prompt for user menu selection
	 */	
	private void askUserForMeuSelection() {
		System.out.print(ENTER_SELECTION);		
	}	
			
	/**
	 * prompt users and capture input 
	 */
	private void captureMenuSelection() {
		try {
			this.userchoice = Integer.parseInt(this.userKeyboard.nextLine());
		} catch (NumberFormatException error) {
			System.out.println("\nOur appoligies. [ " + this.getErrorString(error) + " ] is not a valid selection.");
		}
	}

	/**
	 * prompt users and capture input 
	 */
	private void matchSelectionToAppFeaturs() {
		switch (this.userchoice) {
			case 1:	
	        	this.viewInventory();
	        	this.displayMenuCaptureInput();
				break;
	        case 2: 
	        	this.addToShoppingCart();
	        	this.displayMenuCaptureInput();
	            break;
	        case 3: 
	        	this.viewCart();
	        	this.displayMenuCaptureInput();
	            break;
	        case 4: 	    		
	        	this.viewBalance();
	        	this.displayMenuCaptureInput();	        		        	
	            break;
	        case 5: 
	        	this.checkOut();
	        	this.displayMenuCaptureInput();
	            break;
	        case 6: 	    			        	
	        	this.returnToMainKiosk();	        	
	            break;	            
	        default: 
	        	System.out.print(ERROR_NOT_INT);	        	
	        	this.displayMenuCaptureInput();
	        	break;		
		}	
	}	

	/**
	 * displays inventory
	 **/	
	private void viewInventory() {
    	System.out.println(MENU_1);		
		System.out.println(this.inventory.toString() + "\n");
	}
	
	/**
	 * add item to shopping cart
	 **/	
	private void addToShoppingCart() {
    	System.out.println(MENU_2);		
		String sku = "";
		int units = 0;			

		System.out.print("\t SKU: ");
		sku = this.userKeyboard.nextLine();
		
		while (sku.length() > 6) {
			System.out.print("\t" + ERROR_SKU);
			sku = this.userKeyboard.nextLine();			
		}

		while (!(units > 0 && units < 1000)) {
			System.out.print("\t Units: ");
			try {
				units = Integer.parseInt(this.userKeyboard.nextLine());
			} catch (NumberFormatException error) {
				System.out.println("\t   > Sorry: [ " + this.getErrorString(error) + ERROR_PRICE);
			}
        }				
		this.customer.addToCart(this.inventory.findComputer(sku), units);		
		System.out.println("\t   > You attempted to add " + units + " "  + this.inventory.findComputer(sku).getSku() + "(s) to cart");		
		System.out.println(SPACE);    	
	}
	
	/**
	 * describe method:
	 **/	
	private void viewCart() {
    	System.out.println(MENU_3);
		System.out.println(this.customer.getCart() + "\n");
	}
	
	/**
	 * describe method:
	 **/	
	private void viewBalance() {
    	System.out.println(MENU_4);
		System.out.println("\t $" + this.customer.getBudget() + "\n");
	}	
	
	/**
	 * describe method:
	 **/	
	private void checkOut() {
    	System.out.println(MENU_5);	
    	this.customer.completePurchase();
	}
	
	/**
	 * return to main menu
	 **/	
	private void returnToMainKiosk() {
		System.out.println(MENU_6);
		System.out.println(THANK_YOU);	
		KioskTUI viewKiosk = new KioskTUI(this.inventory);		
		viewKiosk.runKiosk();    	
	}		
		
	/**
	 * provides specific text that causes input errors 
	 * @param error accepts ERROR from try catch 
	 * @return returns the value\input responsible for error
	 */
	private String getErrorString(NumberFormatException error) {
		int first = error.getMessage().indexOf("\"", 0) + 1; 
		int second = error.getMessage().lastIndexOf("\"");

		return error.getMessage().substring(first, second);		
	}	
 

}
