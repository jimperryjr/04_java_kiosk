package edu.westga.cs6311.kiosk.controller;


import edu.westga.cs6311.kiosk.model.InventoryManager;
import edu.westga.cs6311.kiosk.view.KioskTUI;

/**
 * Final Exam: unit tests for program
 * @author 	CS6311
 * @author 	jim perry, jr.
 * @version	10.17.2015
 */
public class GreatPurchaseDriver {
		
	/**
	 * Entry point of the application
	 * @param args not used
	 * @precondition None
	 */
	public static void main(String[] args) {
		InventoryManager inventory = new InventoryManager();	
		KioskTUI viewKiosk = new KioskTUI(inventory);		
		viewKiosk.runKiosk();
	}
}
